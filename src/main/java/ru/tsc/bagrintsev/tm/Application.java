package ru.tsc.bagrintsev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.component.Backup;
import ru.tsc.bagrintsev.tm.component.Bootstrap;

import java.io.IOException;

/**
 * @author Sergey Bagrintsev
 * @version 1.28.1
 */

public final class Application {

    public static void main(@Nullable final String[] args) throws IOException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
