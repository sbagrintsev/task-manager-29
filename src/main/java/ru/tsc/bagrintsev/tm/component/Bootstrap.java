package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.command.data.AbstractDataCommand;
import ru.tsc.bagrintsev.tm.command.data.BackupLoadCommand;
import ru.tsc.bagrintsev.tm.command.data.DataBase64LoadCommand;
import ru.tsc.bagrintsev.tm.command.data.DataBinaryLoadCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.SystemUtil;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Set;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    final Backup backup = new Backup(this);

    {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.bagrintsev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        classes.forEach(this::registry);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
        registry(command);
    }

    public void run(@Nullable final String[] args) throws IOException {
        initPID();
        initLogger();
        try {
            processOnStart(args);
        } catch (AbstractException | GeneralSecurityException e) {
            loggerService.error(e);
        }
        try {
            initUsers();
        } catch (GeneralSecurityException | AbstractException e) {
            System.err.println("User initialization error...");
            loggerService.error(e);
        }
        if (!loadSavedData()) {
            try {
                initDemoData();
            } catch (AbstractException e) {
                System.err.println("Data initialization error...");
                loggerService.error(e);
            }
        }
        initBackup();
        while (true) {
            try {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                @NotNull final String command = TerminalUtil.nextLine();
                processOnTheGo(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initBackup() {
        backup.start();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() throws AbstractException {
        userRepository.findAll()
                .forEach(user -> {
                    try {
                        initUserData(user.getLogin());
                    } catch (AbstractException e) {
                        e.printStackTrace();
                    }
                });
    }

    private boolean loadSavedData() {
        final boolean checkBackup = Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP));
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBackup) {
            processOnTheGo(BackupLoadCommand.NAME, false);
        } else if (checkBinary) {
            processOnTheGo(DataBinaryLoadCommand.NAME, false);
        } else if (checkBase64) {
            processOnTheGo(DataBase64LoadCommand.NAME, false);
        }
        return checkBackup || checkBinary || checkBase64;
    }

    private void initUserData(String login) throws AbstractException {
        @NotNull final String userId = userRepository.findByLogin(login).getId();
        taskService.create(userId, String.format("%s first task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s second task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s third task", login), String.format("%s simple description", login));
        taskService.create(userId, String.format("%s fourth task", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s first project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s second project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s third project", login), String.format("%s simple description", login));
        projectService.create(userId, String.format("%s fourth project", login), String.format("%s simple description", login));
    }

    private void initLogger() {
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** Task Manager is shutting down ***");
            }
        });
    }

    private void initUsers() throws GeneralSecurityException, AbstractException {
        userService.create("test", "test").setEmail("test@test.ru");
        userService.create("admin", "admin").setRole(Role.ADMIN);
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String arg) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByShort(arg);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String[] args) throws AbstractException, GeneralSecurityException {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        processOnStart(arg);
        commandService.getCommandByName("exit").execute();
    }

    @SneakyThrows
    private void processOnTheGo(@Nullable final String command) {
        processOnTheGo(command, true);
    }

    @SneakyThrows
    protected void processOnTheGo(@Nullable final String command, boolean checkRoles) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
