package ru.tsc.bagrintsev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.data.BackupSaveCommand;

public final class Backup extends Thread{

    @NotNull
    private  final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(3000);
            bootstrap.processOnTheGo(BackupSaveCommand.NAME, false);
        }
    }

}
