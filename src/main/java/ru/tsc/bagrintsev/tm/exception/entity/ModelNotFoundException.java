package ru.tsc.bagrintsev.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}
