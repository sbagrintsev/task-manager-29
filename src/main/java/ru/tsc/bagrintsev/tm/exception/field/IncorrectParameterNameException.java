package ru.tsc.bagrintsev.tm.exception.field;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;

public final class IncorrectParameterNameException extends AbstractFieldException {

    public IncorrectParameterNameException(
            @Nullable final EntityField paramName,
            @Nullable final String entityName
    ) {
        super(String.format("Error! There is no parameter: %s on entity: %s...", paramName, entityName));
    }

}
