package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataJaxbXmlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-jaxb-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in xml file";
    }

}
