package ru.tsc.bagrintsev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.io.FileOutputStream;

public final class DataJacksonJsonSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_JSON)) {
            fos.write(json.getBytes());
            fos.flush();
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in json file";
    }

}
