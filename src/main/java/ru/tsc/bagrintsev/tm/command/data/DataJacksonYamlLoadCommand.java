package ru.tsc.bagrintsev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJacksonYamlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_YAML)));
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from yaml file";
    }

}
