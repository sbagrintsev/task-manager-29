package ru.tsc.bagrintsev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.INDEX);
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        showParameterInfo(EntityField.NAME);
        @Nullable final String name = TerminalUtil.nextLine();
        showParameterInfo(EntityField.DESCRIPTION);
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by index.";
    }

}
