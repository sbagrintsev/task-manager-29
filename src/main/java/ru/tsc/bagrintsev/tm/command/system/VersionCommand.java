package ru.tsc.bagrintsev.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class VersionCommand extends AbstractSystemCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        System.out.printf("task-manager version: %s%n", getPropertyService().getApplicationVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print version.";
    }

}
