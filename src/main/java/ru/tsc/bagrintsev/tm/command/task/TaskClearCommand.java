package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @Nullable final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}
