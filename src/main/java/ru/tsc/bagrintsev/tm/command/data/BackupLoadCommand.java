package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public final class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        @NotNull final byte[] bytes = Base64.getDecoder().decode(base64);
        try (@NotNull final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             @NotNull final ObjectInputStream ois = new ObjectInputStream(bais)) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from backup";
    }

}
