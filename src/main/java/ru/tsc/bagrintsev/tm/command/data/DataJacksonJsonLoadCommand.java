package ru.tsc.bagrintsev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJacksonJsonLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from json file";
    }

}
