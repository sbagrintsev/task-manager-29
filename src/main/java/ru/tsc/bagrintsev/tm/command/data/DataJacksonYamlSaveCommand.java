package ru.tsc.bagrintsev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.io.FileOutputStream;

public final class DataJacksonYamlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_YAML)) {
            fos.write(yaml.getBytes());
            fos.flush();
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-yaml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in yaml file";
    }

}
